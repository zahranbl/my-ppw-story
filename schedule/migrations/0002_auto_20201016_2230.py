# Generated by Django 3.1.1 on 2020-10-16 15:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='tahun',
            field=models.CharField(choices=[('Ganjil 2020/2021', 'Ganjil 2020/2021'), ('Genap 2020/2021', 'Genap 2020/2021')], default='GENAP', max_length=16),
        ),
    ]
