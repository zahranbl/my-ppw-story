from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.index, name='profile'),
    path('halamanbaru/', views.profile, name='halamanbaru'),
]